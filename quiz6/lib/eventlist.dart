import 'package:flutter/material.dart';
import 'package:quiz6/konten.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class EventList extends StatelessWidget {
  List<Konten> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Konten>((json) => Konten.fromJson(json)).toList();
}
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 275,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: content.length,
        itemBuilder: (BuildContext context, int index){
          return Card(
          shadowColor: Colors.grey.withOpacity(.2),
          elevation: 2,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(8))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(
                  content[index].urlImage,
                  fit: BoxFit.cover,
                  height: 170,
                ).cornerRadiusWithClipRRectOnly(
                  topLeft: 8,
                  topRight: 8,
                ),
                8.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      content[index].category,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 16,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    4.height,
                    Text(
                      content[index].title,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      content[index].isiKonten,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 12,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],)
              ],
            ).paddingOnly(left:8, right:8),
          ));
        }
      ),
    );
  }
}