import 'package:flutter/material.dart';
import 'package:quiz6/model.dart';
import 'package:quiz6/repository.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Album> listAlbum = [];
  Repository repository= Repository();

  getData() async {
    listAlbum = await repository.getData();
  }

  void initState(){
    getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.separated(
        itemBuilder: (context, index){
          return Container(
            child: Text(listAlbum[index].name),
          );
        },
        separatorBuilder: (context, index){
          return Divider();
        },
        itemCount: listAlbum.length,
      )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
